/*-----------------------------
 * GUESS A WORD function.
 * ------------------------*/
(function() {
    'use strict';

    var initialWordsSet = [];

    function hangman(words) {

        words = words || initialWordsSet;

        var word = pickWord(words);

        if(!word) {
            return;
        }

        words = words.filter(function(x) {
            return x !== word;
        });

        var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

        $('.guess').remove();
        $('.letter').remove();

        var totalChances = 6;
        var maxChances = 6;
        var openedLetters = 0;

        $('.chance-value').text(totalChances - maxChances + '/' + maxChances);

        var $alphabet = $('#alphabet');
        alphabet.forEach(function(x) {
            $alphabet.append($('<span class="guess">' + x + '</span>'));
        });

        var wordLength = word.length;
        var wordLetters = word.split('');

        for(var i=0; i<wordLength; i++) {
            $('#word').append($('<span class="letter">_</span>'));
        }

        $('.guess').click(function() {
            var count = 0;
            for (var i = 0; i < wordLength; i++) {
                if (wordLetters[i] == $(this).text()) {
                    $('.letter').eq(i).text($(this).text());
                    count++;
                }
            }

            openedLetters += count;

            if (!count) {
                maxChances = maxChances - 1;
                humanDraw(maxChances);
            }

            $('.chance-value').text(totalChances - maxChances + '/' + totalChances);
            $(this).css('visibility', (count > 0 ? 'hidden' : 'hidden')).unbind('click');

            /*------------------------
             * Display red score block if you lose
             ------------------------*/
            if (!maxChances) {
                $('#score-value-block').css('background', '#f0596f').append($('<span class="score-value">' + parseInt(openedLetters / wordLength * 100, 10) + '%</span>'));
                $('.score-list').append($('<li style="background: #f0596f"><a>' + word + ' ' + parseInt(openedLetters / wordLength * 100, 10) + '%</a></li>'));
                $('.lose-win').text("LOSE");
                $('.guess').unbind('click');
                $('.play-again-button').css('visibility', 'visible');

                for (var i = 0; i < wordLength; i++) {
                    if ($('.letter').eq(i).text() != wordLetters[i]) {
                        $('.letter').eq(i).text(wordLetters[i]);
                        $('.letter').eq(i).css('color', '#414456');
                    }
                }

            }
            /*------------------------
             * Display green score block if you win
             ------------------------*/
            if (openedLetters == wordLength) {
                $('#score-value-block').css('background', '#48ea9f').append($('<span class="score-value">' + parseInt(maxChances / totalChances * 100, 10) + '%</span>'));
                $('.score-list').append($('<li style="background: #48ea9f"><a>' + word + ' ' + parseInt(maxChances / totalChances * 100, 10) + '%</a></li>'));
                $('.lose-win').text("WIN");
                $('.guess').unbind('click');
                $('.play-again-button').css('visibility', 'visible');
            }
        });

    }

    /*-------------------------------------
     * DRAW ELEMENTS USING CANVAS
     --------------------------------------*/
    function gibbetDraw() {
        var object = document.getElementById("canvas");
        var context = object.getContext("2d");
        context.beginPath();
        context.moveTo(50, 390);
        context.quadraticCurveTo(150, 250, 210, 390);
        context.moveTo(140, 320);
        context.lineTo(145, 20);
        context.moveTo(120, 20);
        context.quadraticCurveTo(200, 90, 350, 30);
        context.moveTo(300, 43);
        context.lineTo(300, 110);
        context.lineWidth = 10;
        // line color
        context.strokeStyle = "white";
        context.stroke();

    }

    function humanDraw(max_chance_number) {
        var object = document.getElementById("canvas");
        var context = object.getContext("2d");
        context.beginPath();
        switch (max_chance_number) {
            /* draw head */
            case 5 :
                context.arc(300, 140, 30, 0, Math.PI * 2, false);
                context.closePath();
                context.stroke();
                break;
            case 4 :
                context.moveTo(300, 170);
                context.quadraticCurveTo(320, 200, 300, 270);
                context.stroke();
                break;
            case 3 :
                context.moveTo(310, 190);
                context.quadraticCurveTo(280, 220, 250, 210);
                context.stroke();
                break;
            case 2 :
                context.moveTo(310, 190);
                context.quadraticCurveTo(330, 220, 370, 230);
                context.stroke();
                break;
            case 1 :
                context.moveTo(302, 265);
                context.quadraticCurveTo(250, 300, 260, 350);
                context.stroke();
                break;
            case 0 :
                context.moveTo(302, 265);
                context.quadraticCurveTo(350, 300, 350, 340);
                context.stroke();
                break;

        }

    }

    function clearDrawArea() {
        var object = document.getElementById("canvas");
        var context = object.getContext("2d");
        context.beginPath();
        context.clearRect(245, 105.1, 150, 290);
    }

    function pickWord(list) {

        var word = null;

        if(!list || list.length == 0) {
            alert('List of friends\' names is empty');
        } else {
            word = list[Math.floor(Math.random() * list.length)].toUpperCase();
        }

        return word;
    }

    function initFB() {
        window.fbAsyncInit = function() {
            FB.init({
                appId : '265535743612006',
                status : true,
                xfbml : true
            });
        };
        ( function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    function getFriends(callback) {
        FB.api('/me/friends?fields=last_name', function(response) {
            if (response.data) {
                var list = response.data.map(function(x) {
                    return x.last_name.toUpperCase();
                });
                initialWordsSet = list;
                callback(list);
            } else {
                alert("Error!");
            }
        });
    }

    function login(callback) {
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                getFriends(callback)
            } else {
                // the user is logged in to Facebook,
                // but has not authenticated your app
                FB.login(function(response) {
                    getFriends(callback)
                });
            }
        });
    }


    $(document).ready(function() {

        initFB();

        $('#start-button').click(function() {

            login(function(list) {
                hangman(list);
            });

            $('.start-button').css('display', 'none');
            $('#hangman').css('display', 'block');

            /* Drawing general elements(gibbet)*/
            gibbetDraw();

        });
        /*------------------------
         * Replay the game.
         ------------------------*/
        $('.play-again-button').on('click', function() {
            $('.score-value').remove();
            $('.lose-win').text("");
            $('.play-again-button').css('visibility', 'hidden');
            clearDrawArea();
            hangman();
        });
    });
})();



